//
//  ItemDetailViewController.swift
//  Checklists
//
//  Created by Carolina Arcos on 11/27/17.
//  Copyright © 2017 Condor Labs. All rights reserved.
//

import UIKit
import UserNotifications

protocol ItemDetailViewControllerDelegate: class {
    
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController)
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishAdding item: ChecklistItem) //add
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishEditing item: ChecklistItem) //edit
}

class ItemDetailViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var doneBarButtom: UIBarButtonItem!
    @IBOutlet weak var itemText: UITextField!
    @IBOutlet weak var shouldRemindSwitch: UISwitch!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var datePickercell: UITableViewCell!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    weak var delegate: ItemDetailViewControllerDelegate?
    var itemToEdit: ChecklistItem?
    var dueDate = Date()
    var datePickerVisible = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //navigationItem.largeTitleDisplayMode = .never
        
        //Static cell
        if let item = itemToEdit { //unwrap the optional --> also with variable shadowing: if let itemToEdit = itemToEdit ...
            title = "Edit Item"
            itemText.text = item.text
            doneBarButtom.isEnabled = true
            shouldRemindSwitch.isOn = item.shouldRemind
            dueDate = item.dueDate
        }
        
        updateDueDateLabel()
    }
    
    // MARK: - Actions
    
    @IBAction func cancel() {
        delegate!.itemDetailViewControllerDidCancel(self)
    }
    
    @IBAction func done() {
        //Editing
        if let itemToEdit = itemToEdit { //variable shadowing
            itemToEdit.text = itemText.text!
            itemToEdit.shouldRemind = shouldRemindSwitch.isOn
            itemToEdit.dueDate = dueDate
            delegate?.itemDetailViewController(self, didFinishEditing: itemToEdit)
            itemToEdit.scheduleNotification()
        }
        //Adding
        else {
            let item = ChecklistItem()
            item.text = itemText.text!
            item.checked = false
            item.shouldRemind = shouldRemindSwitch.isOn
            item.dueDate = dueDate
            
            delegate?.itemDetailViewController(self, didFinishAdding: item)
            item.scheduleNotification()
        }
    }
    
    @IBAction func dateChanged(_ datePicker: UIDatePicker) {
        dueDate = datePicker.date
        updateDueDateLabel()
    }
    
    // MARK: - DueDate and Notifications
    
    func updateDueDateLabel() {
        let formtatter = DateFormatter()
        formtatter.dateStyle =  .medium
        formtatter.timeStyle = .short
        dueDateLabel.text = formtatter.string(from: dueDate)
    }
    
    func showDatePicker() {
        datePickerVisible = true
        
        let indexPathDateRow = IndexPath(row: 1, section: 1)
        let indexPathDatePicker = IndexPath(row: 2, section: 1)
        
        if let dateCell = tableView.cellForRow(at: indexPathDateRow) {
            dateCell.detailTextLabel!.textColor = dateCell.detailTextLabel!.tintColor
        }
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPathDatePicker], with: .fade)
        tableView.reloadRows(at: [indexPathDateRow], with: .none)
        tableView.endUpdates()
        
        datePicker.setDate(dueDate, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 && indexPath.row == 2 {
            return datePickercell
        } else {
            return super.tableView(tableView, cellForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 && datePickerVisible {
            return 3
        } else {
            return super.tableView(tableView, numberOfRowsInSection: section) //original data source
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row == 2 {
            return 217
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        itemText.resignFirstResponder()
        
        if indexPath.section == 1 && indexPath.row == 1 {
            if !datePickerVisible {
                showDatePicker()
            } else {
                hideDatePicker()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        var newIndexPath = indexPath
        if indexPath.section == 1 && indexPath.row == 2 {
            newIndexPath = IndexPath(row: 0, section: indexPath.section)
        }
        return super.tableView(tableView, indentationLevelForRowAt: newIndexPath)
    }
    
    func hideDatePicker() {
        if datePickerVisible {
            datePickerVisible = false
        }
        
        let indexPathDateRow = IndexPath(row: 1, section: 1)
        let indexPathDatePicker = IndexPath(row: 2, section: 1)
        
        if let cell = tableView.cellForRow(at: indexPathDateRow) {
            cell.detailTextLabel!.textColor = UIColor.black
        }
        
        tableView.performBatchUpdates({
            tableView.reloadRows(at: [indexPathDateRow], with: .none)
            tableView.deleteRows(at: [indexPathDatePicker], with: .fade)
        })
//        tableView.beginUpdates()
//        tableView.reloadRows(at: [indexPathDateRow], with: .none)
//        tableView.deleteRows(at: [indexPathDatePicker], with: .fade)
//        tableView.endUpdates()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if datePickerVisible {
            hideDatePicker()
        }
    }
    
    @IBAction func shouldRemindToggled(_ switchControl: UISwitch) {
        itemText.resignFirstResponder()
        if switchControl.isOn {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) {
                granted, error in
                // ...
            }
        }
    }
    
    // MARK: -
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 && indexPath.row == 1 {
            return indexPath
        } else {
            return nil //disable selections for rows
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        itemText.becomeFirstResponder() //show up the keyboard automatically
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let oldText = itemText.text!
        let stringRange = Range(range, in: oldText)!
        let newText = oldText.replacingCharacters(in: stringRange, with: string)
        
        doneBarButtom.isEnabled = !newText.isEmpty
        /*
         if newText.isEmpty {
            doneBarButtom.isEnabled = false
        } else {
            doneBarButtom.isEnabled = true
        }
        */
        return true
    }
}
