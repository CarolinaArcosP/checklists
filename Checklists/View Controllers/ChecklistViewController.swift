//
//  ViewController.swift
//  Checklists
//
//  Created by Carolina Arcos on 11/24/17.
//  Copyright © 2017 Condor Labs. All rights reserved.
//

import UIKit
import CoreGraphics

class ChecklistViewController: UITableViewController, ItemDetailViewControllerDelegate {

    var checklist: Checklist!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        title = checklist.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //tableView(_:numberOfRowsInSection:)
    override func tableView(_ tableView: UITableView, numberOfRowsInSection selection: Int) -> Int {
        return checklist.items.count //Number of rows in table
    }
    
    //tableView(_:cellForRowAt:)
    //Rows are the data, cells are the views
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        /*/
        //Prototype cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath) //ChecklistItem identifier
         
        let item = checklist.items[indexPath.row]
         
        configureText(for: cell, with: item)
        configureCheckmark(for: cell, with: item)
         */
        
        
        // TODO: -
        let cell = makeCell(for: tableView)
        
        let item = checklist.items[indexPath.row]
        cell.textLabel!.text = item.text
        cell.accessoryType = .detailDisclosureButton
        cell.imageView!.image = item.checked ? UIImage(named: "checkmark") : UIImage(named: "No Icon")
        
        let formtatter = DateFormatter()
        formtatter.dateStyle =  .medium
        formtatter.timeStyle = .short
        cell.detailTextLabel!.text = formtatter.string(from: item.dueDate)
        
        return cell
    }
    
    func makeCell(for tableView: UITableView) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
            return cell
        } else {
            return UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
    }
    
    //tableView(_:didSelectRowAt:)
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            let item = checklist.items[indexPath.row]
            item.toggleChecked()
            configureCheckmark(for: cell, with: item)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //Enables swipe-to-delete
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        checklist.items.remove(at: indexPath.row)
        
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
    }
    
    //Tell ItemDetailViewController whose his delegate (self-> ChecklistViewController)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddItem" {
            let controller = segue.destination as! ItemDetailViewController
            controller.delegate = self
        } /*else if segue.identifier == "EditItem" {
            let controller = segue.destination as! ItemDetailViewController
            controller.delegate = self
            
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                controller.itemToEdit = checklist.items[indexPath.row]
            }
        }*/
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let controller = storyboard!.instantiateViewController(withIdentifier: "ItemDetailViewController") as! ItemDetailViewController
        controller.delegate = self
        
        let item = checklist.items[indexPath.row]
        controller.itemToEdit = item
        
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func configureCheckmark(for cell: UITableViewCell, with item: ChecklistItem) {
        //let label = cell.viewWithTag(1001) as! UILabel
        
        if item.checked  {
            //label.text = "\u{2714}"
            cell.imageView!.image = UIImage(named: "checkmark")
        } else {
            //label.text = ""
            cell.imageView!.image = UIImage(named: "No Icon")
        }
    }
    
    func configureText(for cell: UITableViewCell, with item: ChecklistItem) {
        let label = cell.viewWithTag(1000) as! UILabel //Ask the cell with tag 1000
        label.text = item.text
    }
    
    
    // MARK: - ItemDetailViewController Protocol
    
    func itemDetailViewControllerDidCancel(_ controller: ItemDetailViewController) {
        navigationController?.popViewController(animated: true) //return to the previous screen
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishAdding item: ChecklistItem) {
        
        checklist.items.append(item) //add to data model
        
        /*
        let newRowIndex = checklist.items.count
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic) //add to table view
         */
        
        checklist.sortItems()
        tableView.reloadData()
        
        navigationController?.popViewController(animated: true) //return to the previous screen
    }
    
    func itemDetailViewController(_ controller: ItemDetailViewController, didFinishEditing item: ChecklistItem) {
        /*
        if let index = checklist.items.index(of: item) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) {
                configureText(for: cell, with: item)
            }
        }
         */
        
        checklist.sortItems()
        tableView.reloadData()
        
        navigationController?.popViewController(animated: true) //return to the previous screen
    }
    
    // MARK: Persistence
    
    /* Get the full path of the Documents folder
     URL -> refer to files in the filesystem file://
    */
    /*func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        // Property List --> XML file format
        return documentsDirectory().appendingPathComponent("Checklist.plist")
    }
    
    func saveChecklistItems() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(items)
            try data.write(to: dataFilePath(), options: Data.WritingOptions.atomic)
        } catch {
            print("Error encoding item array! :(")
        }
    }
    
    func loadChecklistItems() {
        let path = dataFilePath()
        if let data = try? Data(contentsOf: path) {
            let decoder = PropertyListDecoder()
            do {
                items = try decoder.decode([ChecklistItem].self, from: data)
            } catch {
                print("Error decoding item array! :(")
            }
        }
        
    }
 */
    
    
}

