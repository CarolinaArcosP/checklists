//
//  Checklist.swift
//  Checklists
//
//  Created by Carolina Arcos on 11/28/17.
//  Copyright © 2017 Condor Labs. All rights reserved.
//

import UIKit

class Checklist: NSObject, Codable {
    var name = ""
    var iconName = "No Icon"
    var items = [ChecklistItem]()
    
    init(name: String, iconName: String = "No Icon") {
        self.name = name
        self.iconName = iconName
        super.init()
    }
    
    func countUncheckedItems() -> Int {
        return items.reduce(0) { count, item in
            count + (item.checked ? 0 : 1)
        }
    }
    
    func sortItems() {
        items.sort(by: { item1, item2 in
            return item1.dueDate.compare(item2.dueDate) == .orderedAscending
        })
    }
    
}
